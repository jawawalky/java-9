# Java 9 - Installation

## Requirements

Before you start with this course, please check that your system fulfills the following requirements


| Requirement        | Description                                                  | Downloads            |
| ------------------ | ------------------------------------------------------------ | -------------------- |
| Access to Internet | The exercises, solutions, documentations, etc. will be provided by GIT repositories. You should be able to download them to your local system. Furthermore required libraries will be loaded by *Maven* from the *Maven Central* repository. | |
| *GIT*              | *GIT* is a version control system. Many operation systems also provide *GIT* as an installable package, e.g. *Linux*. | [https://git-scm.com/downloads](https://git-scm.com/downloads) |
| *JDK 9+*           | The *JDK* (= *Java Development Kit*) is the basic develeopment and runtime environment for *Java*. | [Oracle](https://www.oracle.com/java/technologies/downloads/)<br/>[OpenJDK](https://openjdk.org/projects/jdk/) |
| *Maven*            | *Maven* is a build tool for *Java* projects. Exercises, demos, etc. will be provided as *Maven* projects. | [https://maven.apache.org](https://maven.apache.org/download.cgi) |
| IDE                | An *IDE* is an *Integrated Development Environment*. All exercises, demos, etc. have been developed in *Eclipse*. Since they are provided as *Maven* projects you can choose any IDE you wish. | [Eclipse](https://www.eclipse.org/downloads/)<br/>[IntelliJ IDEA](https://www.jetbrains.com) |

**Notes**

> Use recent versions of these programs, so they are compatible with the desired *Java* version.

> Make sure you are allowed to access remote *GIT* repositories and the *Maven Central* repository and that you are not blocked by some Internet proxy.

## Program Path

Most of the time we are going to work with the IDE. But certain commands need to be executed from a terminal/shell. All operating systems offer shells, such as *PowerShell* on *Windows*, *bash* on *Linux*, etc.

In order to be able to call the previously mentioned programs from anywhere in your file system, they need to be added to your program path (see `PATH` environment variable).

## Resources

As said, the exercises, solutions, etc. for this course are provided by a *GIT* repository.

You can clone this repository on your own system with the following command

```
git clone https://gitlab.com/jawawalky/java-9.git
```

> Alternatively you can open the link `https://gitlab.com/jawawalky/java-9.git` in your browser and download a certain branch from the respository.

## Testing Installation

### GIT

Open a shell and run the following command

```
git -v
```

It should display something like that

```
git version 2.37.2
```

### JDK

Open a shell and run the following command

```
java -version
```

It should display something like that

```
openjdk version "17.0.5" 2022-10-18
OpenJDK Runtime Environment (build 17.0.5+8-Ubuntu-2ubuntu1)
OpenJDK 64-Bit Server VM (build 17.0.5+8-Ubuntu-2ubuntu1, mixed mode, sharing)
```

> Output depends much on which JDK you are using, *Oracle*, *OpenJDK*, etc.

### Maven

Open a shell and run the following command

```
mvn -v
```

It should display something like that

```
Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)
Maven home: /opt/apache-maven-3.8.6
Java version: 17.0.5, vendor: Private Build, runtime: /usr/lib/jvm/java-17-openjdk-amd64
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "5.19.0-23-generic", arch: "amd64", family: "unix"
```

### IDE

Check that your IDE supports *Java* version 9 or higher!



**Enjoy your course!**