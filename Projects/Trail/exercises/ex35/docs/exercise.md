# Logging

## What's it All About?

We want to explore the standardized `Logger` API. The class `System` now has
a method `System.getLogger(String)`, which allows us to retrieve a logger
object. Standard log levels have been introduced and different logging
frameworks can be plugged in.

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex35/Main.java)
and follow the *TODO*s.
