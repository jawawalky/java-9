/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex35;

/**
 * A simple POJO.
 * 
 * @author Franz Tost
 */
public class Person {
	
	// fields /////
	
	private String firstName;
	
	private String lastName;
	
	
	// constructors /////
	
	public Person(final String firstName, final String lastName) {
		
		super();
		
		this.firstName = firstName;
		this.lastName  = lastName;
		
	}

	
	// methods /////

	@Override
	public String toString() {
		
		return this.firstName + " " + this.lastName;
		
	}
	
}
