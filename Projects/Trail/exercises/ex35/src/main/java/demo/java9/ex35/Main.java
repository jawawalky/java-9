/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex35;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import demo.util.Demo;

/**
 * In this demo we learn about the unified logging API of the JDK.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Get a logger for the name 'demo'.
		
		Demo.log("A) Supported log levels ...");
		
		// TODO
		//
		//  o Check, which log levels are supported by the logger.
		//
		//    Hint: Use 'isLoggable(Logger, Level)
		
		
		Demo.log("B) Logger implementation ...");
		
		// TODO
		//
		//  o Print the logger class on the console.
		//
		//  o Log a message with TRACE level.
		//
		//  o Log a message with INFO level.
		
		Demo.log("C) Logger methods ...");
		
		// TODO
		//
		//  o Log a simple text.
		//
		//  o Log an object, e.g. 'Person'.
		//
		//  o Log a text template with parameters.
		//
		//  o Log an exception.
		//
		//  o Log a text produced by a supplier.
		
		Demo.log("Finished.");

	}
	
	private void isLoggable(
		final Logger logger,
		final Level  level
	) {
		
		// TODO
		//
		//  o Write, if the specified log level is supported, to the console.
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
