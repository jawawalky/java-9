# Optional

## What's it All About?

We want to explore the new features of the class `Optional`.

The explored methods are

- `Optional.stream(...)`
- `Optional.or(...)`
- `Optional.ifPresentOrElse(...)`

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex33/Main.java)
and follow the *TODO*s.
