/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex33;

import java.util.Optional;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Optional}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.demoStream();
		this.demoOr();
		this.demoIfPresentOrElse();
		
		Demo.log("Finished.");

	}
	
	private void demoStream() {

		Demo.log("A) Optional.stream() ...");
		
		// TODO
		//
		//  o With a possibility of 50:50 produce a value 'Bingo!' or 'null'.
		//
		//    Hint: Use 'fiftyFifty(...)'.
		//
		//  o Stream the result of the 'Optional' to the console.
		
		
		// TODO
		//
		//  o Generate a stream with ten 'Optional's, which are by chance
		//    (50:50) either 1 or null.
		//
		//  o Calculate the sum of the non-null values.
		
	}
	
	private void demoOr() {

		Demo.log("B) Optional.or(...) ...");
		
		// TODO
		//
		//  o Produce by a chance of 50:50 the value 'one'.
		//
		//  o If the value was 'null', then produce by a chance of 50:50
		//    the value 'two'.
		//
		//  o If the value was 'null', then produce by a chance of 50:50
		//    the value 'three'.
		//
		//  o Finally the value was 'null', then produce the value 'four'.
		
	}
	
	private void demoIfPresentOrElse() {

		Demo.log("C) Optional.ifPresentOrElse(...) ...");
		
		// TODO
		//
		//  Playing Bingo!
		//
		//  o Produce by a chance of 50:50 a random value between 0 and 10.
		//
		//    Hint: Use 'fiftyFifty(...)' and 'Demo.nextInt(...)'.
		//
		//  o If the value is non-null, print the value + ' wins',
		//    otherwise print 'lost'.
		
		this.fiftyFifty(Demo.nextInt(10))
		.ifPresentOrElse(
			v -> Demo.log("  > %d wins", v),
			() -> Demo.log("  > lost")
		);
		
	}
	
	private <T> Optional<T> fiftyFifty(final T value) {
		
		return
			Demo.nextInt(2) == 0
			?
			Optional.of(value)
			:
			Optional.empty();
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
