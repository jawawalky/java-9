package demo.java9.ex03a.api;

import demo.java9.ex03a.model.Temperature;
import demo.java9.ex03a.service.TemperatureServiceImpl;

public interface TemperatureService {
	
	// methods /////
	
	static TemperatureService get() {
		
		return new TemperatureServiceImpl();
		
	}

	Temperature convert(Temperature temperature);

}