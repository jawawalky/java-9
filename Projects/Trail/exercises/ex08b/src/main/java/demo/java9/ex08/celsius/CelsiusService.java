/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex08.celsius;

import demo.java9.ex08.api.TemperatureService;

/**
 * A service for conversions from Fahrenheit to Celsius.
 * 
 * @author Franz Tost
 */
public class CelsiusService implements TemperatureService {

	// methods /////
	
	@Override
	public double convert(final double fahrenheit) {

		return (fahrenheit - 32.0) * 5 / 9;
		
	}
	
}
