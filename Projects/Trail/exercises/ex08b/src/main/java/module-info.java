module demo.java9.ex08.celcius {
	
	requires demo.java9.ex08.api;
	
	exports demo.java9.ex08.celsius;
	
}
