/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex32;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Stream}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.demoTakeWhile();
		this.demoDropWhile();
		this.demoOfNullable();
		this.demoIterate();
		
		Demo.log("Finished.");

	}
	
	private void demoTakeWhile() {

		Demo.log("A) Stream.takeWhile(...) ...");
		
		// TODO
		//
		//  o Create a stream on the sequence
		//    '0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0'.
		//
		//  o Take elements from the stream while they are less than 5.
		//
		//  o Join the result stream to a comma-separated string and
		//    print it on the console.
		
	}
	
	private void demoDropWhile() {

		Demo.log("B) Stream.dropWhile(...) ...");
		
		// TODO
		//
		//  o Create a stream on the sequence
		//    '0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0'.
		//
		//  o Drop elements from the stream while they are less than 5.
		//
		//  o Join the result stream to a comma-separated string and
		//    print it on the console.
		
	}
	
	private void demoOfNullable() {

		Demo.log("C) Stream.ofNullable(...) ...");
		
		// TODO
		//
		//  o Create with a possibility of 50:50 the values 'Bingo!' or
		//    'null'.
		//
		//     Hint: Use 'Demo.nextInt(2)', which creates with a possibility
		//           of 50:50 the values 0 and 1.
		//
		//  o Only print the result, using 'Stream.ofNullable(...)',
		//    if it is not 'null'. 
		
	}
	
	private void demoIterate() {

		Demo.log("D) Stream.iterate(...) ...");
		
		// TODO
		//
		//  o Print the values 2, 4, 8, 16, ... on the console, using
		//    the 'Stream.iterate(...)' method.
		//
		//  o Stop the iteration, when the value become greater than 100. 
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
