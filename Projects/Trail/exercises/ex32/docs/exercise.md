# Streams

## What's it All About?

We want to explore the new methods of the interface `Stream`.

The explored methods are

- `Stream.takeWhile(...)`
- `Stream.dropWhile(...)`
- `Stream.ofNullable(...)`
- `Stream.iterate(...)`

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex32/Main.java)
and follow the *TODO*s.
