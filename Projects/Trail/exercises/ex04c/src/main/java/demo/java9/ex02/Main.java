/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex02;

import java.lang.reflect.Method;

import demo.util.Demo;

/**
 * In this demo we see, how to control reflective access to class members.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.reflectionAnalysis("demo.java9.ex04a.a.A");
		this.reflectionAnalysis("demo.java9.ex04a.b.B");
		this.reflectionAnalysis("demo.java9.ex04a.c.C");
		this.reflectionAnalysis("demo.java9.ex04a.d.D");
		this.reflectionAnalysis("demo.java9.ex04b.e.E");
		this.reflectionAnalysis("demo.java9.ex04b.f.F");
		
		Demo.log("Finished.");

	}
	
	private void reflectionAnalysis(final String className) {
		
		Demo.log("%s%s", className, ":");
		
		try {
			
			final Class<?> type     = this.getType(className);
			final Object   instance = this.createInstance(type);
			
			this.invokeMethod(instance, "doPublic");
			this.invokeMethod(instance, "doPrivate");
			
		} // try
		catch (RuntimeException e) { } // catch
		
	}
	
	private Class<?> getType(final String className) {
		
		try {
			
			return Class.forName(className);
			
		} // try
		catch (ClassNotFoundException e) {
			
			Demo.log("  > not found!");
			throw new RuntimeException();
			
		} // catch
		
	}
	
	private Object createInstance(final Class<?> type) {
		
		try {
			
			return type.getConstructor().newInstance();
			
		} // try
		catch (Exception e) {
			
			Demo.log("  > creation failed!");
			throw new RuntimeException();
			
		} // catch
		
	}
	
	private void invokeMethod(
		final Object instance,
		final String methodName
	) {
		
		try {
			
			final Method method =
				instance.getClass().getDeclaredMethod(methodName);
			
			method.setAccessible(true);
			method.invoke(instance);
			
			Demo.log("  > %s() ok.", methodName);
			
		} // try
		catch (Exception e) {
			
			Demo.log("  > %s() failed!", methodName);
			
		} // catch
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
