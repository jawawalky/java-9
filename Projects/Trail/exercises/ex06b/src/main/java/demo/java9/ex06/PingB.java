/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex06;

/**
 * A simple ping service.
 * 
 * @author Franz Tost
 */
public class PingB {
	
	// methods /////
	
	String ping() {          // <- 'ping()' has package visibility.
                             //    Since the package is shared between
        return "B";          //    several modules, the method is accessible
                             //    in other modules than this.
    }
	
}
