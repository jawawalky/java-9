/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex36;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Stream}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() throws Exception {

		Demo.logWithThread("Running demo ...");
		
		this.demoCompleteOnTimeout();
		this.demoOrTimeout();

		Demo.logWithThread("Finished.");

	}
	
	private void demoCompleteOnTimeout() throws Exception {

		Demo.logWithThread("A) completeOnTimeout(...) ...");

		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> x = new CompletableFuture<>();
		
		// TODO
		//
		//  o Assign to 'y' the calculation, defined by
		//    'createCalculation(...)'. 'x' is the variable, which feeds
		//    the calculation with a value.
		//
		//  o If the calculation has not been completed within 700 ms,
		//    then let the result be 0.
		//
		//  o Complete the calculation for the value '2'.
		//
		//  o Assign the result to 'result'.
		
		Integer result = null;
		
		long end = System.currentTimeMillis();
		
		Demo.logWithThread("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");

	}
	
	private void demoOrTimeout() throws InterruptedException, ExecutionException {

		Demo.logWithThread("B) OrTimeout(...) ...");

		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> x = new CompletableFuture<>();
		
		// TODO
		//
		//  o Assign to 'y' the calculation, defined by
		//    'createCalculation(...)'. 'x' is the variable, which feeds
		//    the calculation with a value.
		//
		//  o If the calculation has not been completed within 700 ms,
		//    then throw a 'TimeoutException'.
		//
		//  o Complete the calculation for the value '2'.
		//
		//  o Assign the result to 'result'.
		//
		//  o Do an appropriate exception handling for a possible
		//    'TimeoutException'.
		
		Integer result = null;
		
		long end = System.currentTimeMillis();
		
		Demo.logWithThread("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private CompletableFuture<Integer> createCalculation(
		final CompletableFuture<Integer> x
	) {
		
		// TODO
		//
		//  o Perform the square operation asynchronously.
		//
		//  o Perform the cube operation asynchronously.
		//
		//  o Add the results of the square and the cube operation and
		//    return the final 'CompletableFuture'.

		return null;
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws Exception {

		new Main().runDemo();

	}

}
