# CompletableFuture

## What's it All About?

The `CompletableFuture` defines some new methods

- `CompletableFuture.completeOnTimeout(...)`
- `CompletableFuture.orTimeout(...)`

These methods allow us to handle operations, which run too long. We can
either handle those situations by providing default values or throwing
exceptions.

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex36/Main.java)
and follow the *TODO*s.
