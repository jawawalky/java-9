package demo.java9.ex08.api;

public interface TemperatureService {
	
	// methods /////
	
	double convert(double temperature);

}