# Flow API

## What's it All About?

The *Flow API* provides interfaces and classes for reactive programming.
It is all based on a publisher-subscriber pattern. A `Publisher` produces
values, which are consumed by a `Subscriber`.

## What to Do?

### The Publisher

Open the file [RandomNumberPublisher.java](../src/main/java/demo/java9/ex20/RandomNumberPublisher.java)
and follow the *TODO*s.

### The Subscriber

Open the file [SumSubscriber.java](../src/main/java/demo/java9/ex20/SumSubscriber.java)
and follow the *TODO*s.

### Putting is All Together

Open the file [Main.java](../src/main/java/demo/java9/ex20/Main.java)
and follow the *TODO*s.
