/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex20;

import java.util.concurrent.Flow.Subscriber;

import demo.util.Demo;

/**
 * A publisher, which publishes random numbers to {@code Subscriber}s.
 * <p>
 * <b>Caution:</b>
 * <blockquote>
 * 	This is not a production-ready publisher! The requirements for
 * 	a correct {@code Publisher} implementation are high. This class is only
 * 	meant for demo purposes.
 * </blockquote>
 * 
 * @author Franz Tost
 */
public class RandomNumberPublisher {
	
	// TODO
	//
	//  o Let this class be a 'Publisher'.
	//
	//  o When a 'Subscriber' subscribes to this publisher, then create
	//    a 'SimpleSubscription'.
	
	
	// inner classes /////
	
	private class SimpleSubscription
		implements
			Runnable
	{
		
		// TODO
		//
		//  o Let this class be a 'Subscription' class.
		//
		//  o Implement the methods of the 'Subscription' interface.
		//    Note: Ignore back-pressure.
		
		
		// fields /////
		
		private volatile Subscriber<? super Integer> subscriber;
		
		
		// constructors /////
		
		public SimpleSubscription(
			final Subscriber<? super Integer> subscriber
		) {
			
			super();
			
			this.subscriber = subscriber;
			
			// TODO
			//
			//  o Pass this 'Subscription' to the 'Subscriber'.
			
			new Thread(this).start();       // <- Starts itself on
				                            //    a different thread.
			
		}
		
		// methods /////

		@Override
		public void run() {
			
			// TODO
			//
			//  o Generate ten random numbers between 0 and 9 and
			//    publish them.
			
			
			final Subscriber<? super Integer> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				// TODO
				//
				//  o Signal the 'Subscriber' that this publisher has
				//    terminated.
				//
				//  o Cancel this subscription.
				
			} // if
			
		}
		
		private void publish(final int randomNumber) {
			
			final Subscriber<? super Integer> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				Demo.sleep(100, 500);       // <- Publish random value with
				                            //    some delay.
				
				// TODO
				//
				//  o Pass the random number to the 'Subscriber'.
				
			} // if
			
		}
		
	}
	
	
	// methods /////

}
