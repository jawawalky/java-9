/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex07b;

import demo.java9.ex07a.personal.PersonalHelloService;

/**
 * An <i>hello</i> service using the {@code PersonalHelloService}.
 * 
 * @author Franz Tost
 */
public class HelloService {
	
	// fields /////
	
	private PersonalHelloService personalHelloService =
		new PersonalHelloService();
	
	
	// methods /////
	
	public String sayHello(final String name) {
		
		return this.personalHelloService.sayHello(name);

	}
	
}
