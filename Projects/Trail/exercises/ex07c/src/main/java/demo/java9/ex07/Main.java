/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex07;

import demo.java9.ex07a.common.CommonHelloService;
import demo.java9.ex07a.personal.PersonalHelloService;
import demo.java9.ex07b.HelloService;
import demo.util.Demo;

/**
 * In this demo we learn, how we can restrict exports of certain packages
 * to certain modules only.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log("A) CommonHelloService ...");

		final CommonHelloService commonHelloService =
			new CommonHelloService();
		
		Demo.log(commonHelloService.sayHello());
		
		
		Demo.log("B) HelloService ...");

		final HelloService helloService = new HelloService();
		
		Demo.log(helloService.sayHello("Duke"));
		
		
		Demo.log("C) PersonalHelloService ...");

		final PersonalHelloService personalHelloService =
		    new PersonalHelloService();
		
		Demo.log(personalHelloService.sayHello("Alice"));
		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
