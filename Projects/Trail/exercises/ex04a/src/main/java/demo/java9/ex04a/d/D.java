/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex04a.d;

/**
 * A class with a public and a private member.
 * 
 * @author Franz Tost
 */
public class D {
	
	// methods /////
	
	public void doPublic() { }
	
	@SuppressWarnings("unused")
	private void doPrivate() { }
	
}
