# SubmissionPublisher

## What's it All About?

The JDK ships with a `Publisher` implementation, `SubmissionPublisher`. It can
be used as a standard publisher, but also as a base class for own publisher
implementations or processor implementations.

## What to Do?

Delete the class [BasePublisher.java](../src/main/java/demo/java9/ex22/BasePublisher.java)
and follow the *TODO*s.

Replace all occurrences of `BasePublisher` by the `SubmissionPublisher` and
correct the code, where necessary.
