/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex22;

import java.util.concurrent.Flow.Processor;
import java.util.concurrent.Flow.Subscription;

import demo.util.Demo;

/**
 * A processor, which generates random numbers.
 * 
 * @author Franz Tost
 */
public class RandomNumberGenerator
	extends
		BasePublisher<Integer>
	implements
		Processor<Boolean, Integer>
{
	
	// fields /////
	
	private Subscription subscription;
	
	
	// methods /////

	@Override
	public void onSubscribe(final Subscription subscription) {
		
		this.subscription = subscription;
		subscription.request(1);
		
	}


	@Override
	public void onNext(final Boolean event) {
		
		this.next(Demo.nextInt(10));
		this.subscription.request(1);
		
	}


	@Override
	public void onError(final Throwable cause) {
		
		this.error(cause);
		
	}


	@Override
	public void onComplete() {
		
		this.complete();
		
	}

}
