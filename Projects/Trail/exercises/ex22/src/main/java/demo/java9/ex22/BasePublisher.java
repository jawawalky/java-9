/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex22;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow.Publisher;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

/**
 * A basic publisher.
 * <p>
 * <b>Caution:</b>
 * <blockquote>
 * 	This is not a production-ready publisher! The requirements for
 * 	a correct {@code Publisher} implementation are high. This class is only
 * 	meant for demo purposes.
 * </blockquote>
 * 
 * @author Franz Tost
 */
public abstract class BasePublisher<T> implements Publisher<T> {
	
	// inner classes /////
	
	public class BaseSubscription implements Subscription {
		
		// fields /////
		
		private volatile Subscriber<? super T> subscriber;
		
		
		// constructors /////
		
		public BaseSubscription(
			final Subscriber<? super T> subscriber
		) {
			
			super();
			
			this.subscriber = subscriber;
			subscriptions.add(this);
			subscriber.onSubscribe(this);
			
		}
		
		// methods /////

		@Override
		public void request(final long n) {
			
			// <- Ignoring back-pressure by doing nothing here.
			
		}

		@Override
		public void cancel() {
			
			subscriptions.remove(this);
			this.subscriber = null;
			
		}

		private void next(final T value) {
			
			final Subscriber<? super T> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				subscriber.onNext(value);
				
			} // if
			
		}
		
		private void error(final Throwable cause) {
			
			final Subscriber<? super T> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				subscriber.onError(cause);
				
			} // if
			
		}
		
		private void complete() {
			
			final Subscriber<? super T> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				subscriber.onComplete();
				
			} // if
			
		}
		
	}
	
	
	// fields /////
	
	private List<BaseSubscription> subscriptions = new ArrayList<>();
	
	
	// methods /////

	@Override
	public void subscribe(final Subscriber<? super T> subscriber) {
		
		new BaseSubscription(subscriber);
		
	}

	protected void next(final T value) {
		
		new ArrayList<>(this.subscriptions).forEach(s -> s.next(value));
		
	}
	
	protected void error(final Throwable cause) {
		
		new ArrayList<>(this.subscriptions).forEach(s -> s.error(cause));
		
	}
	
	protected void complete() {
		
		new ArrayList<>(this.subscriptions).forEach(s -> s.complete());
		
	}
	
}
