# StackWalker

## What's it All About?

We want to explore the new class `StackWalker`. It allows us to analyze
the call stack of a method. We will use it to find the number or recursive
invocations of the method 'Main.call()'.

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex34/Main.java)
and follow the *TODO*s.
