/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex34;

import demo.util.Demo;

/**
 * In this demo we learn about the {@code StackWalker}.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.call();
		
		Demo.log("Finished.");

	}
	
	private void call() {
		
		Demo.log("  > Call ...");
		
		// TODO
		//
		//  o Let this method call itself recursively, if it hasn't failed.
		//
		//    Hint: Use 'failed()' to determine, if the method has failed or
		//          not.
		//
		//  o In case the method has failed, then use the 'StackWalker' to
		//    determine, how often the method 'call()' was called recursively
		//    before it failed.
		//
		//    Hint: You can use an 'AtomicInteger' for counting.
		
	}
	
	private boolean failed() {
		
		return Demo.nextInt(10) < 2;
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
