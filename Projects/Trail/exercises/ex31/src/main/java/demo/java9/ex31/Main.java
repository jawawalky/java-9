/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex31;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import demo.util.Demo;

/**
 * In this demo we learn about starting external processes, accessing the data
 * of their current state and stopping them programmatically.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo()
		throws IOException, InterruptedException, ExecutionException
	{

		Demo.log("Running demo ...");
		
		this.printCurrentProcessHandleAndInfo();
		this.listAllProcesses();
		this.startProcessAndWaitForTermination();
		this.startAndTerminateProcessWithCompletableFuture();
		
		Demo.log("Finished.");

	}
	
	private void printCurrentProcessHandleAndInfo() {

		Demo.log("A) Process handle and info of current process ...");
		
		// TODO
		//
		//  o Get a handle to the current process and print
		//
		//      o the process ID,
		//      o if the process is alive,
		//      o the parent of the process, if there is one,
		//      o the child processes, if there are any,
		//      o any descendants and
		//      o if the process support normal termination.
		
		
		// TODO
		//
		//  o Get the process info and print
		//
		//      o the command that triggered the process,
		//      o the command line,
		//      o the command arguments,
		//      o the start time of the process,
		//      o the its CPU time,
		//      o the user, who triggered the process.
		
		Demo.println();
		Demo.println();
		
	}
	
	private void listAllProcesses() {

		Demo.log("B) List of all processes ...");
		
		// TODO
		//
		//  o Use 'ProcessHandle' to find all processes.
		//
		//  o Filter out processes, which have no command.
		//
		//  o List them with their process ID and their command.
		
		Demo.println();
		Demo.println();
		
	}
	
	private void startProcessAndWaitForTermination()
		throws IOException, InterruptedException
	{

		Demo.log("C) Starting process ...");
		
		final String processName = "gnome-calculator";
		// final String processName = "calc.exe";
		
		// TODO
		//
		//  o Use a 'ProcessBuilder' to start the process.
		//
		//  o Get the process info of the started process and print
		//    its command.
		//
		//  o Get a 'ProcesHandle' to the process by its ID and check,
		//    if the process is alive.
		//
		//  o Wait for the process to be terminated and print the termination
		//    code.
		
		
		Demo.println();
		Demo.println();
		
	}
	
	private void startAndTerminateProcessWithCompletableFuture()
		throws IOException, InterruptedException, ExecutionException
	{

		Demo.log("D) Starting and stopping process programmatically ...");
		
		final String processName = "gnome-calculator";
		// final String processName = "calc.exe";
		
		// TODO
		//
		//  o Use a 'ProcessBuilder' to start the process.
		//
		//  o We can get a 'CompletableFuture', which will be completed,
		//    when the process is terminated.
		//
		//  o Use the 'CompletableFuture' to print a message, when
		//    the process has terminated.
		//
		//  o Let the current process sleep for 5 seconds.
		//    (Hint: Demo.sleep(5000))
		//
		//  o Get a handle to the started process.
		//
		//  o Use the handle to terminate/destroy the process.
		//
		//  o Let the current process sleep for 1 second, so the process
		//    has time to terminate.
		//    (Hint: Demo.sleep(1000))
		
		Demo.println();
		Demo.println();
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 * 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	public static void main(String[] args)
		throws IOException, InterruptedException, ExecutionException
	{

		new Main().runDemo();

	}

}
