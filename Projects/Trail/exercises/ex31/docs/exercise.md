# Process Handles

## What's it All About?

With the new interface `ProcessHandle` we can get detailed information about
a running processes.

In this exercise we learn about

- the evaluation of the process information,
- listing available processes,
- starting a process and waiting for its termination and
- starting and terminating a process with `CompletableFuture`.

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/ex31/Main.java)
and follow the *TODO*s.
