/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex21;

/**
 * A processor, which generates random numbers.
 * 
 * @author Franz Tost
 */
public class RandomNumberGenerator {
	
	// TODO
	//
	//  o Let this class be a 'Processor'. Later it should receive events from
	//    the 'Trigger' and produce random number. So consider what must be
	//    the generic types.
	//
	//    Hint: Use the 'BasePublisher' to implement the 'Publisher' part
	//          of the 'Processor'.
	//
	//  o Implement the required methods.
	//
	//  o The processor should produce a random number between 0 and 9, when
	//    it receives an event from the trigger.
	
	
	// fields /////
	
	
	// methods /////

}
