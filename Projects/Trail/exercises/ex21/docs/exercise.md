# Processors

## What's it All About?

`Processor`s are both `Publisher` and `Subscriber`. So they can be plugged in
between a `Publisher` and a `Subscriber`. Normally a processor receives
an object/event from a publisher, modifies it and passes it on to a subscriber.

## What to Do?

Open the file [RandomNumberGenerator.java](../src/main/java/demo/java9/ex21/RandomNumberGenerator.java)
and follow the *TODO*s.

Open the file [Main.java](../src/main/java/demo/java9/ex21/Main.java)
and follow the *TODO*s.
