/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex07a.common;

/**
 * A common <i>hello</i> service.
 * 
 * @author Franz Tost
 */
public class CommonHelloService {
	
	// methods /////
	
	public String sayHello() {
		
		return "Hi guys!";

	}
	
}
