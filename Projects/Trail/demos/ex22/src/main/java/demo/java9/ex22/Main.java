/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex22;

import demo.util.Demo;

/**
 * In this demo we learn how to use a flow {@code Processor}.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.logWithThread("Running demo ...");
		
		@SuppressWarnings("resource")
		final Trigger publisher = new Trigger();
		final RandomNumberGenerator processor = new RandomNumberGenerator();
		final SumSubscriber subscriber = new SumSubscriber();
		
		publisher.subscribe(processor);
		processor.subscribe(subscriber);
		
		publisher.start();
		
		Demo.logWithThread("Please wait ...");
		
		Demo.sleep(5000);

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
