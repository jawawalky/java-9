/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex22;

import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The trigger is a publisher, which emits events at random times. The emitted
 * event is just a {@code Boolean} value of {@code Boolean#TRUE}.
 * 
 * @author Franz Tost
 */
public class Trigger
	extends
		SubmissionPublisher<Boolean>
	implements
		Runnable
{
	
	// methods /////
	
	public void start() {
		
		new Thread(this).start();
		
	}

	@Override
	public void run() {
		
		Stream.generate(() -> Boolean.TRUE)
			.limit(10)
			.forEach(this::publish);
		
		this.close();
		
	}
	
	private void publish(final boolean value) {
		
		Demo.sleep(100, 500);
		this.submit(value);
		
	}
	
}
