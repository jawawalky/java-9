/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex22;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import demo.util.Demo;

/**
 * A subscriber, which calculates the sum of the random numbers published
 * by the {@link Trigger}.
 * 
 * @author Franz Tost
 */
public class SumSubscriber implements Subscriber<Integer> {
	
	// fields /////
	
	private Subscription subscription;
	
	private int sum;
	
	
	// methods /////

	@Override
	public void onSubscribe(final Subscription subscription) {
		
		this.subscription = subscription;
		subscription.request(1);
		
	}

	@Override
	public void onNext(final Integer randomNumber) {
		
		Demo.logWithThread("Next: %d", randomNumber);
		this.sum += randomNumber;
		this.subscription.request(1);
		
	}

	@Override
	public void onError(final Throwable cause) {
		
		Demo.logWithThread("Error: %s", cause.getMessage());
		
	}

	@Override
	public void onComplete() {
		
		Demo.logWithThread("The sum is %d.", this.sum);
		
		this.subscription.cancel();
		this.subscription = null;
		
	}
	
}
