/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex05a;

import static demo.java9.ex05a.TemperatureUnit.CELSIUS;

/**
 * A temperature value.
 * 
 * @author Franz Tost
 */
public class Temperature {
	
	// fields /////
	
	private double value;
	
	private TemperatureUnit unit;
	
	
	// constructors /////
	
	public Temperature(
		final double          value,
		final TemperatureUnit unit
	) {
		
		super();
		
		this.value = value;
		this.unit  = unit;
		
	}
	

	// methods /////
	
	public double getValue() {
		
		return this.value;
		
	}
	
	public TemperatureUnit getUnit() {
		
		return this.unit;
		
	}

	@Override
	public String toString() {
		
		return
			String.format(
				"%.1f°%s",
				this.value,
				(CELSIUS.equals(this.unit) ? "C" : "F")
			);

	}
	
}
