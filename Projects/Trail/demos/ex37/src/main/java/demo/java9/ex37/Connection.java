/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex37;

import demo.util.Demo;

/**
 * A class representing some connection, which can be opened and closed.
 */
public class Connection {
	
	// fields /////
	
	private String name;
	
	
	// constructors /////
	
	public Connection(final String name) {
		
		super();
		
		this.name = name;
		
	}
	
	
	// methods /////
	
	public void open() {
		
		Demo.logWithThread("Connection %s opened ...", this.name);
		
	}
	
	public void close() {
		
		Demo.logWithThread("... connection %s closed.", this.name);
		
	}
	
}
