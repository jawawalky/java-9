/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex37;

/**
 * A container for a resource value.
 */
public abstract class Resource<T> implements Runnable {
	
	// fields /////
	
	private T value;
	
	
	// constructors /////
	
	public Resource(final T value) {
		
		super();
		
		this.value = value;
		
	}
	
	
	// methods /////
	
	protected T getValue() {
		
		return this.value;
		
	}
	
}
