/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex37;

import java.lang.ref.Cleaner;

/**
 * A class representing some connection, which can be opened and closed.
 */
public class Connector {
	
	// inner classes /////
	
	private static class ConnectionCloser extends Resource<Connection> {

		// fields /////
		
		public ConnectionCloser(final Connection connection) {
			
			super(connection);
			
		}

		@Override
		public void run() {
			
			this.getValue().close();
			
		}
		
	}
	
	// fields /////
	
	private Connection connectionA = new Connection("A");
	
	private Connection connectionB = new Connection("B");
	
	
	// constructors /////
	
	public Connector() {
		
		super();
		
		this.connectionA.open();
		this.connectionB.open();
		
		Cleaner.create().register(
			this,
			new ConnectionCloser(this.connectionA)
		);
		
		Cleaner.create().register(
			this,
			new ConnectionCloser(this.connectionB)
		);
			
	}
	
}
