package demo.java9.ex08.api;

import java.util.ServiceLoader;

public interface TemperatureService {
	
	// methods /////
	
	static TemperatureService get() {
		
		return ServiceLoader.load(TemperatureService.class).findFirst().get();
		
	}

	double convert(double temperature);

}