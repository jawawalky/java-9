module demo.java9.ex04a {
	
	exports demo.java9.ex04a.a;
	opens demo.java9.ex04a.a;
	
	exports demo.java9.ex04a.b;
	
	opens demo.java9.ex04a.c;
	
}
