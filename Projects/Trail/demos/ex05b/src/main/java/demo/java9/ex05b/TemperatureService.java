/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex05b;

import static demo.java9.ex05a.TemperatureUnit.CELSIUS;
import static demo.java9.ex05a.TemperatureUnit.FAHRENHEIT;

import demo.java9.ex05a.Temperature;

/**
 * A service for temperature conversions.
 * 
 * @author Franz Tost
 */
public class TemperatureService {

	// methods /////

	public Temperature convert(final Temperature temperature) {
		
		Temperature result = null;
		
		switch (temperature.getUnit()) {
		
		case CELSIUS:
			result = new Temperature(
				temperature.getValue() * 9 / 5 + 32.0,
				FAHRENHEIT
			);
			break;
			
		case FAHRENHEIT:
			result = new Temperature(
				(temperature.getValue() - 32.0) * 5 / 9,
				CELSIUS
			);
			break;
		
		} // switch
		
		return result;
		
	}
	
}
