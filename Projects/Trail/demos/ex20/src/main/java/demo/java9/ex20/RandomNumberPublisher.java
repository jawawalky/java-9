/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex20;

import java.util.concurrent.Flow.Publisher;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * A publisher, which publishes random numbers to {@code Subscriber}s.
 * <p>
 * <b>Caution:</b>
 * <blockquote>
 * 	This is not a production-ready publisher! The requirements for
 * 	a correct {@code Publisher} implementation are high. This class is only
 * 	meant for demo purposes.
 * </blockquote>
 * 
 * @author Franz Tost
 */
public class RandomNumberPublisher implements Publisher<Integer> {
	
	// inner classes /////
	
	private class SimpleSubscription
		implements
			Subscription,
			Runnable
	{
		
		// fields /////
		
		private volatile Subscriber<? super Integer> subscriber;
		
		
		// constructors /////
		
		public SimpleSubscription(
			final Subscriber<? super Integer> subscriber
		) {
			
			super();
			
			this.subscriber = subscriber;
			subscriber.onSubscribe(this);
			
			new Thread(this).start();       // <- Starts itself on
				                            //    a different thread.
			
		}
		
		// methods /////

		@Override
		public void request(final long n) {
			
			// <- Ignoring back-pressure by doing nothing here.
			
		}

		@Override
		public void cancel() {
			
			// <- Cancel subscription to this subscriber.
			
			this.subscriber = null;
			
		}

		@Override
		public void run() {
			
			Stream.generate(() -> Demo.nextInt(10))   // <- Generate ten
				.limit(10)                            //    random numbers,
				.forEach(this::publish);              //    each between 0 and
			                                          //    9. Publish them to
			                                          //    the subscriber.
			
			final Subscriber<? super Integer> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				subscriber.onComplete();
				this.cancel();
				
			} // if
			
		}
		
		private void publish(final int randomNumber) {
			
			final Subscriber<? super Integer> subscriber = this.subscriber;
			
			if (subscriber != null) {
				
				Demo.sleep(100, 500);       // <- Publish random value with
				                            //    some delay.
				
				subscriber.onNext(randomNumber);
				
			} // if
			
		}
		
	}
	
	
	// methods /////

	@Override
	public void subscribe(final Subscriber<? super Integer> subscriber) {
		
		new SimpleSubscription(subscriber); // <- When a subscriber
		                                    //    registers to this publisher,
		                                    //    then a subscription is passed
		                                    //    to the subscriber. It allows
		                                    //    the subscriber to notify
		                                    //    the publisher about
		                                    //    back-pressure and to cancel
		                                    //    the subscription.
		
	}

}
