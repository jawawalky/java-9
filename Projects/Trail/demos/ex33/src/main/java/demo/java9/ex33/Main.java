/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex33;

import java.util.Optional;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Optional}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.demoStream();
		this.demoOr();
		this.demoIfPresentOrElse();
		
		Demo.log("Finished.");

	}
	
	private void demoStream() {

		Demo.log("A) Optional.stream() ...");
		
		this.fiftyFifty("  > Bingo!")
			.stream()
			.forEach(Demo::log);
		
		Demo.log(
			"  > %d",
			Stream.generate(() -> this.fiftyFifty(1))
				.limit(10)
				.flatMap(Optional::stream)
				.reduce(0, (x, y) -> x + y)
		);
		
	}
	
	private void demoOr() {

		Demo.log("B) Optional.or(...) ...");
		
		Demo.log(
			"  > %s",
			this.fiftyFifty("one")
			.or(() -> this.fiftyFifty("two"))
			.or(() -> this.fiftyFifty("three"))
			.orElse("four")
		);
		
	}
	
	private void demoIfPresentOrElse() {

		Demo.log("C) Optional.ifPresentOrElse(...) ...");
		
		this.fiftyFifty(Demo.nextInt(10))
		.ifPresentOrElse(
			v -> Demo.log("  > %d wins", v),
			() -> Demo.log("  > lost")
		);
		
	}
	
	private <T> Optional<T> fiftyFifty(final T value) {
		
		return
			Demo.nextInt(2) == 0
			?
			Optional.of(value)
			:
			Optional.empty();
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
