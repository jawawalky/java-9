module demo.java9.ex02b {
	
	requires transitive demo.java9.ex02a;
	
	exports demo.java9.ex02b;
	
}
