/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex36;

import demo.util.Demo;

/**
 * Mathematical operations.
 */
public final class Math {
	
	// methods /////
	
	public static int add(final int a, final int b) {
		
		Demo.sleep(100, 200);
		return a + b;
		
	}
	
	public static int mlt(final int a, final int b) {
		
		Demo.sleep(200, 400);
		return a * b;
		
	}
	
	public static int square(final int x) {
		
		Demo.logWithThread("square(%d)", x);
		return mlt(x, x);
		
	}
	
	public static int cube(final int x) {
		
		Demo.logWithThread("cube(%d)", x);
		return mlt(x, square(x));
		
	}
	
}
