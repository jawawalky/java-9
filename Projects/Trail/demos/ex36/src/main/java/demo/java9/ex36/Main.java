/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex36;

import static java.util.concurrent.TimeUnit.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Stream}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() throws Exception {

		Demo.logWithThread("Running demo ...");
		
		this.demoCompleteOnTimeout();
		this.demoOrTimeout();

		Demo.logWithThread("Finished.");

	}
	
	private void demoCompleteOnTimeout() throws Exception {

		Demo.logWithThread("A) completeOnTimeout(...) ...");

		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> x = new CompletableFuture<>();
		final CompletableFuture<Integer> y =
			this.createCalculation(x)
				.completeOnTimeout(0, 700, MILLISECONDS);
		
		x.complete(2);
		
		Integer result = y.get();
		
		long end = System.currentTimeMillis();
		
		Demo.logWithThread("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");

	}
	
	private void demoOrTimeout() throws InterruptedException, ExecutionException {

		try {
			
			Demo.logWithThread("B) OrTimeout(...) ...");

			long start = System.currentTimeMillis();
			
			final CompletableFuture<Integer> x = new CompletableFuture<>();
			final CompletableFuture<Integer> y =
				this.createCalculation(x)
					.orTimeout(700, MILLISECONDS);
			
			x.complete(2);
			
			Integer result = y.get();
			
			long end = System.currentTimeMillis();
			
			Demo.logWithThread("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
		} // try
		catch (ExecutionException e) {
			
			if (e.getCause() instanceof TimeoutException) {
				
				Demo.logWithThread("Calculation took too long!");
				
			} // if
			else {
				
				throw e;
				
			} // else
			
		} // catch

	}
	
	private CompletableFuture<Integer> createCalculation(
		final CompletableFuture<Integer> x
	) {

		final CompletableFuture<Integer> f1 = x.thenApplyAsync(Math::square);
		final CompletableFuture<Integer> f2 = x.thenApplyAsync(Math::cube);
		return f1.thenCombine(f2, Math::add);
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws Exception {

		new Main().runDemo();

	}

}
