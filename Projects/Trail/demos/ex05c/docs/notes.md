# Notes

## Disable Workspace Resolution

We need to turn off *Workspace Resolution*, since it interferes with
the resolution of automatic modules.

1. Go to the *Project Explorer*.
1. Right-click on the project to open the context menu.
1. Select *Maven > Disable Workspace Resolution*.

> That will add the generated JAR files as dependencies and not the *Eclipse* projects themselves. That allows for the correct resolution of the names of
the automatic modules.

