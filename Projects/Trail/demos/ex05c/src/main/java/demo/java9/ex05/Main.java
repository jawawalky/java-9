/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex05;

import static demo.java9.ex05a.TemperatureUnit.CELSIUS;
import static demo.java9.ex05a.TemperatureUnit.FAHRENHEIT;

import demo.java9.ex05a.Temperature;
import demo.java9.ex05b.TemperatureService;
import demo.util.Demo;

/**
 * In this demo we learn, how to use automatic modules.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final Temperature c0   = new Temperature(0, CELSIUS);
		final Temperature c100 = new Temperature(100, CELSIUS);
		
		final Temperature f0   = new Temperature(0, FAHRENHEIT);
		final Temperature f100 = new Temperature(100, FAHRENHEIT);
		
		final TemperatureService temperatureService = new TemperatureService();
		
		Demo.log("0°C = " + temperatureService.convert(c0));
		Demo.log("100°C = " + temperatureService.convert(c100));
		
		Demo.log("0°F = " + temperatureService.convert(f0));
		Demo.log("100°F = " + temperatureService.convert(f100));
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
