/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex34;

import java.util.concurrent.atomic.AtomicInteger;

import demo.util.Demo;

/**
 * In this demo we learn about the {@code StackWalker}.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.call();
		
		Demo.log("Finished.");

	}
	
	private void call() {
		
		Demo.log("  > Call ...");
		
		if (!this.failed()) {
			
			this.call();
			
		} // if
		else {
			
			final AtomicInteger depth = new AtomicInteger(0);
			
			StackWalker.getInstance()
				.forEach(
					stackFrame -> {
						if ("call".equals(stackFrame.getMethodName())) {
							depth.incrementAndGet();
						} // if
					}
				);
			
			Demo.log("  > Failed at level %d!", depth.get());
			
		} // else
		
	}
	
	private boolean failed() {
		
		return Demo.nextInt(10) < 2;
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
