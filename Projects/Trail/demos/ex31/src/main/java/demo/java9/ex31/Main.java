/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex31;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import demo.util.Demo;

/**
 * In this demo we learn about starting external processes, accessing the data
 * of their current state and stopping them programmatically.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo()
		throws IOException, InterruptedException, ExecutionException
	{

		Demo.log("Running demo ...");
		
		this.printCurrentProcessHandleAndInfo();
		this.listAllProcesses();
		this.startProcessAndWaitForTermination();
		this.startAndTerminateProcessWithCompletableFuture();
		
		Demo.log("Finished.");

	}
	
	private void printCurrentProcessHandleAndInfo() {

		Demo.log("A) Process handle and info of current process ...");
		
		final ProcessHandle handle = ProcessHandle.current();
		
		Demo.println("Process Handle:");
		Demo.println("  PID:          " + handle.pid());
		Demo.println("  Alive:        " + handle.isAlive());
		Demo.println("  Parent:       " + handle.parent().map(v -> v.toString()).orElse("---"));
		Demo.println("  Children:     " + handle.children().collect(toList()));
		Demo.println("  Descendants:  " + handle.descendants().collect(toList()));
		Demo.println("  Normal Term.: " + handle.supportsNormalTermination());
		Demo.println();
		
		final ProcessHandle.Info info = handle.info();
		
		Demo.println("Process Info:");
		Demo.println("  Command:        " + info.command().orElse("---"));
		Demo.println("  Command Line:   " + info.commandLine().orElse("---"));
		Demo.println("  Arguments:      " + info.arguments().map(v -> v.toString()).orElse("[]"));
		Demo.println("  Start:          " + info.startInstant().map(v -> v.toString()).orElse("---"));
		Demo.println("  Total CPU Time: " + info.totalCpuDuration().map(v -> v.toString()).orElse("---"));
		Demo.println("  User:           " + info.user().orElse("---"));
		Demo.println();
		Demo.println();

	}
	
	private void listAllProcesses() {

		Demo.log("B) List of all processes ...");
		
		ProcessHandle.allProcesses()
			.filter(handle -> handle.info().command().isPresent())
			.forEach(handle ->
				Demo.log(
					"%d > %s",
					handle.pid(),
					handle.info().command().orElse("---")
				)
			);
		
		Demo.println();
		Demo.println();
		
	}
	
	private void startProcessAndWaitForTermination()
		throws IOException, InterruptedException
	{

		Demo.log("C) Starting process ...");
		
		final String processName = "gnome-calculator";
		// final String processName = "calc.exe";
		
		final Process process = new ProcessBuilder(processName).start();
		final ProcessHandle.Info info = process.info();
		
		Demo.log("  Process %s is running ...", info.command());
		
		final Optional<ProcessHandle> handleRef =
			ProcessHandle.of(process.pid());
		
		if (handleRef.isPresent()) {
			
			final ProcessHandle handle = handleRef.get();
			Demo.log(
				"  ... and is %s.",
				(handle.isAlive() ? "alive" : "dead")
			);
			
		} // if
		
		int result = process.waitFor();
		Demo.log(
			"  Process %d has been terminated with code %d.",
			process.pid(),
			result
		);
		
		Demo.println();
		Demo.println();
		
	}
	
	private void startAndTerminateProcessWithCompletableFuture()
		throws IOException, InterruptedException, ExecutionException
	{

		Demo.log("D) Starting and stopping process programmatically ...");
		
		final String processName = "gnome-calculator";
		// final String processName = "calc.exe";
		
		Process process = new ProcessBuilder(processName).start();
		Demo.log("  Starting process %d ...", process.pid());
		
		process.onExit()
			.thenAcceptAsync(
				p -> Demo.log("  Process %d has terminated.", p.pid())
			);
		
		Demo.log("  Terminating process in 5s ...");
		Demo.sleep(5000);
		
		final Optional<ProcessHandle> handle = ProcessHandle.of(process.pid());
		
		if (handle.isPresent()) {
			
			Demo.log("  ... initiated.");
			boolean done = handle.get().destroy();
			Demo.log("  -> %s", done ? "ok" : "failed");
			
		} // if
		
		Demo.sleep(1000);
		Demo.println();
		Demo.println();
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 * 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	public static void main(String[] args)
		throws IOException, InterruptedException, ExecutionException
	{

		new Main().runDemo();

	}

}
