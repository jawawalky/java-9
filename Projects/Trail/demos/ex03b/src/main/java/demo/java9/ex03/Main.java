/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex03;

import static demo.java9.ex03a.model.TemperatureUnit.CELSIUS;
import static demo.java9.ex03a.model.TemperatureUnit.FAHRENHEIT;

import demo.java9.ex03a.api.TemperatureService;
import demo.java9.ex03a.model.Temperature;
import demo.util.Demo;

/**
 * In this demo we learn, how to export only selected packages from our module.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final TemperatureService service = TemperatureService.get();
		
		Demo.log("0°C = " + service.convert(new Temperature(0, CELSIUS)));
		Demo.log("0°F = " + service.convert(new Temperature(0, FAHRENHEIT)));
		
		// <- 'TemperatureServiceImpl' is not accessible here!
		//
		// final TemperatureServiceImpl serviceImpl = new TemperatureServiceImpl();
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
