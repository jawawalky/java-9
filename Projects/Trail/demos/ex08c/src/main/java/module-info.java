module demo.java9.ex08.fahrenheit {
	
	requires demo.java9.ex08.api;
	
	exports demo.java9.ex08.fahrenheit;
	
	provides demo.java9.ex08.api.TemperatureService
	with demo.java9.ex08.fahrenheit.FahrenheitService;
	
}
