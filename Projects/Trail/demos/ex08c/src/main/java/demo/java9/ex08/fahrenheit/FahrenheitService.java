/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex08.fahrenheit;

import demo.java9.ex08.api.TemperatureService;

/**
 * A service for conversions from Fahrenheit to Celsius.
 * 
 * @author Franz Tost
 */
public class FahrenheitService implements TemperatureService {

	// methods /////
	
	@Override
	public double convert(final double celsius) {

		return celsius * 9 / 5 + 32.0;
			
	}
	
}
