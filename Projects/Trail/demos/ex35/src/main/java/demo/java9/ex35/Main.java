/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex35;

import static java.lang.System.Logger.Level.DEBUG;
import static java.lang.System.Logger.Level.ERROR;
import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.TRACE;
import static java.lang.System.Logger.Level.WARNING;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import demo.util.Demo;

/**
 * In this demo we learn about the unified logging API of the JDK.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final Logger logger = System.getLogger("demo");
		
		Demo.log("A) Supported log levels ...");
		
		this.isLoggable(logger, TRACE);
		this.isLoggable(logger, DEBUG);
		this.isLoggable(logger, INFO);
		this.isLoggable(logger, WARNING);
		this.isLoggable(logger, ERROR);
		
		Demo.log("B) Logger implementation ...");
		
		Demo.log("  > %s", logger.getClass().getName());
		logger.log(TRACE, "I am a trace.");
		logger.log(INFO,  "I am a text.");
		
		Demo.log("C) Logger methods ...");
		
		logger.log(INFO, "I am a text.");                  // <- Pure text
		logger.log(INFO, new Person("Duke", "Java"));      // <- Object
		logger.log(INFO, "I am a {0} template.", "text");  // <- Text template
		logger.log(                                        // <- Error message
			ERROR,                                         //    with cause
			"I'm an error.",
			new Exception("Because something went wrong!")
		);
		logger.log(INFO, () -> "I am a message supplier.");// <- A message supplier,
		                                                   //    which only evaluates
		                                                   //    the message, if it
		                                                   //    is really logged.
		
		// TODO
		//
		//  Resource bundle.
		
		Demo.log("Finished.");

	}
	
	private void isLoggable(
		final Logger logger,
		final Level  level
	) {
		
		Demo.log(
			"  > %s: %s",
			level.getName(),
			String.valueOf(logger.isLoggable(level))
		);
			
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
