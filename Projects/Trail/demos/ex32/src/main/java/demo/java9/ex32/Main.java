/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex32;

import static java.util.stream.Collectors.joining;

import java.util.stream.Stream;

import demo.util.Demo;

/**
 * In this demo we learn about the new features of {@code Stream}s.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.demoTakeWhile();
		this.demoDropWhile();
		this.demoOfNullable();
		this.demoIterate();
		
		Demo.log("Finished.");

	}
	
	private void demoTakeWhile() {

		Demo.log("A) Stream.takeWhile(...) ...");
		
		final String result =
			Stream.of(0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0)
				.takeWhile(v -> v < 5)
				.map(v -> String.valueOf(v))
				.collect(joining(", "));
		
		Demo.log("  > %s", result);
		
	}
	
	private void demoDropWhile() {

		Demo.log("B) Stream.dropWhile(...) ...");
		
		final String result =
			Stream.of(0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0)
				.dropWhile(v -> v < 5)
				.map(v -> String.valueOf(v))
				.collect(joining(", "));
		
		Demo.log("  > %s", result);
		
	}
	
	private void demoOfNullable() {

		Demo.log("C) Stream.ofNullable(...) ...");
		
		Stream.ofNullable(Demo.nextInt(2) == 0 ? "  > Bingo!" : null)
			.forEach(Demo::log);
		
	}
	
	private void demoIterate() {

		Demo.log("D) Stream.iterate(...) ...");
		
		Demo.log(
			"  > %s",
			Stream.iterate(2, i -> i < 100, i -> i * 2)
				.map(v -> String.valueOf(v))
				.collect(joining(", "))
		);
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
