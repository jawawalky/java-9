/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.ex03a.service;

import static demo.java9.ex03a.model.TemperatureUnit.CELSIUS;
import static demo.java9.ex03a.model.TemperatureUnit.FAHRENHEIT;

import demo.java9.ex03a.api.TemperatureService;
import demo.java9.ex03a.model.Temperature;

/**
 * A service for temperature conversions for internal usage.
 * 
 * @author Franz Tost
 */
public class TemperatureServiceImpl implements TemperatureService {

	// methods /////
	
	@Override
	public Temperature convert(final Temperature temperature) {
		
		Temperature result = null;
		
		switch (temperature.getUnit()) {
		
		case CELSIUS:
			result = new Temperature(
				temperature.getValue() * 9 / 5 + 32.0,
				FAHRENHEIT
			);
			break;
			
		case FAHRENHEIT:
			result = new Temperature(
				(temperature.getValue() - 32.0) * 5 / 9,
				CELSIUS
			);
			break;
		
		} // switch
		
		return result;
		
	}
	
}
