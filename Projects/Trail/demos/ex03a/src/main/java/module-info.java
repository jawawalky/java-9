module demo.java9.ex03a {
	
	exports demo.java9.ex03a.model;
	exports demo.java9.ex03a.api;
	
	//  Note:
	//
	// Only these two packages are exported from this module. Other modules
	// can see elements of those packages, but they cannot see elements of
	// internal packages, such as 'demo.java9.ex03a.service', which contains
	// the implementation of the service.
	// That allows us to export the contract (= the interface) of a service,
	// but to hide its implementation.
	
}
