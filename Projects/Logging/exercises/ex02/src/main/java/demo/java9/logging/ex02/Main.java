/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex02;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import demo.util.Demo;

/**
 * In this demo we learn about the unified logging API of the JDK.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Get the system logger for 'A' or alternatively for 'B'.

		
		Demo.log("A) Supported log levels ...");
		
		// TODO
		//
		//  o Print, which log levels are supported by the logger.

		
		Demo.log("B) Logger implementation ...");
		
		// TODO
		//
		//  o Print the class of the logger on the console.
		//
		//  o Write some log messages with different log levels.

		
		Demo.log("Finished.");

	}
	
	private void isLoggable(
		final Logger logger,
		final Level  level
	) {
		
		Demo.log(
			"  > %s: %s",
			level.getName(),
			String.valueOf(logger.isLoggable(level))
		);
			
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
