# Logger Finder

## What's it All About?

The JDK ships with a default implementation for loggers. We can change that
and provide a custom implementation. So in this exercise we will implement our
own loggers and a `LoggerFinder`, which provides `Logger` instances.
The logger implementation is defined in one module and can be used in another
module.

## What to Do?

Open the file [Main.java](../src/main/java/demo/java9/logging/ex02/DemoLoggerFinder.java)
and follow the *TODO*s.

Now go to

[module-info.java](../src/main/java/module-info.java)

and activate the service loading mechanism for `System.LoggerFinder`.

Test your program.