# Logger Finder

## What's it All About?

The JDK ships with a default implementation for loggers. We can change that
and provide a custom implementation. So in this exercise we will implement our
own loggers and a `LoggerFinder`, which provides `Logger` instances.

## What to Do?

### Implementing a Logger

We will provide two different logger implementations

[LoggerA.java](../src/main/java/demo/java9/logging/ex01/LoggerA.java)

and

[LoggerB.java](../src/main/java/demo/java9/logging/ex01/LoggerB.java)

Follow the *TODOs* in both classes.

> **Note:** The implementations should be pretty simple, without any fancy
details.

### Implementing a LoggerFinder

A `LoggerFinder` is an object, which provides `Logger` instances.

Open the file [Main.java](../src/main/java/demo/java9/logging/ex01/DemoLoggerFinder.java)
and follow the *TODO*s.

### Module Info

Let the module provide the `System.LoggerFinder` by its implementation
`DemoLoggerFinder`.

[module-info.java](../src/main/java/module-info.java)

### Using the LoggerFinder

The second part of this exercise consists of a module, which uses
the `LoggerFinder` that we created in this module. This is done in the next
exercise *Java 9 [Logging] (02), Using Logger Exercise*. So open that
exercise an proceed there.
