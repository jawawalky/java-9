/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex01;

/**
 * A {@code LoggerFinder} implementation.
 * 
 * @author Franz Tost
 */
public class DemoLoggerFinder {
	
	// TODO
	//
	//  o Let this class be a 'LoggerFinder'.
	//
	//  o If the logger name is 'A', then return a logger instance
	//    of type 'LoggerA'.
	//
	//  o If the logger name is 'B', then return a logger instance
	//    of type 'LoggerB'.
	//
	//  o Otherwise return 'null'.

	// methods /////

}
