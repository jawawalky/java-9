/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex01;

/**
 * A simple {@code Logger} implementation.
 * 
 * @author Franz Tost
 */
public class LoggerB {

	// TODO
	//
	//  o Let this class be a 'Logger' class.
	//
	//  o Implement the required logger methods with the following requirements
	//
	//      o The name of the logger should be 'B'.
	//
	//      o All levels above and equal to INFO should be logged.
	//
	//      o Log all messages in the following format
	//
	//          [B] > <message>
	

	// methods /////

}
