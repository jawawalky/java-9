/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex02;

import static java.lang.System.Logger.Level.DEBUG;
import static java.lang.System.Logger.Level.ERROR;
import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.TRACE;
import static java.lang.System.Logger.Level.WARNING;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import demo.util.Demo;

/**
 * In this demo we learn about the unified logging API of the JDK.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");

		final Logger logger = System.getLogger("A");
//		final Logger logger = System.getLogger("B");
		
		Demo.log("A) Supported log levels ...");
		
		this.isLoggable(logger, TRACE);
		this.isLoggable(logger, DEBUG);
		this.isLoggable(logger, INFO);
		this.isLoggable(logger, WARNING);
		this.isLoggable(logger, ERROR);
		
		Demo.log("B) Logger implementation ...");
		
		Demo.log("  > %s", logger.getClass().getName());
		logger.log(TRACE, "I am a trace.");
		logger.log(INFO,  "I am a text.");
		
		Demo.log("Finished.");

	}
	
	private void isLoggable(
		final Logger logger,
		final Level  level
	) {
		
		Demo.log(
			"  > %s: %s",
			level.getName(),
			String.valueOf(logger.isLoggable(level))
		);
			
	}
	
//	private LoggerFinder getLoggerFinder() {
//		
//		final ServiceLoader<LoggerFinder> serviceLoader =
//			ServiceLoader.load(LoggerFinder.class);
//		
//		return serviceLoader.findFirst().get();
//		
//	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
