/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex01;

import java.lang.System.Logger;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import demo.util.Demo;

/**
 * A simple {@code Logger} implementation.
 * 
 * @author Franz Tost
 */
public class LoggerA implements Logger {

	// methods /////

	@Override
	public String getName() {
		
		return "A";
		
	}

	@Override
	public boolean isLoggable(final Level level) {
		
		return true;    // <- Let all levels be loggable.
		
	}

	@Override
	public void log(
		final Level          level,
		final ResourceBundle bundle,
		final String         msg,
		final Throwable      thrown
	) {
		
		Demo.log("[A] > %s", msg);
		
	}

	@Override
	public void log(
		final Level          level,
		final ResourceBundle bundle,
		final String         format,
		final Object...      params
	) {
		
		Demo.log("[A] > %s", MessageFormat.format(format, params));
		
	}
	
}
