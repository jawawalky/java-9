/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex01;

import java.lang.System.Logger;
import java.lang.System.LoggerFinder;

/**
 * A {@code LoggerFinder} implementation.
 * 
 * @author Franz Tost
 */
public class DemoLoggerFinder extends LoggerFinder {

	// methods /////

	@Override
	public Logger getLogger(
		final String name,
		final Module module
	) {
		
		switch (name) {
			case "A":	return new LoggerA();
			case "B":	return new LoggerB();
			default:	return null;
		} // switch
		
	}
	
}
