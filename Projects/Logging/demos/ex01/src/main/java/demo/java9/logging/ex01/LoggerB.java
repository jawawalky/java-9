/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java9.logging.ex01;

import static java.lang.System.Logger.Level.INFO;

import java.lang.System.Logger;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import demo.util.Demo;

/**
 * A simple {@code Logger} implementation.
 * 
 * @author Franz Tost
 */
public class LoggerB implements Logger {

	// methods /////

	@Override
	public String getName() {
		
		return "B";
		
	}

	@Override
	public boolean isLoggable(final Level level) {
		
		return INFO.ordinal() <= level.ordinal();     // <- Only messages with
		                                              //    level INFO or above
		                                              //    are logged.
		
	}

	@Override
	public void log(
		final Level          level,
		final ResourceBundle bundle,
		final String         msg,
		final Throwable      thrown
	) {
		
		if (INFO.ordinal() <= level.ordinal()) {
			
			Demo.log("[B] > %s", msg);
			
		} // if
		
	}

	@Override
	public void log(
		final Level          level,
		final ResourceBundle bundle,
		final String         format,
		final Object...      params
	) {
		
		if (INFO.ordinal() <= level.ordinal()) {
			
			Demo.log("[B] > %s", MessageFormat.format(format, params));
			
		} // if
		
	}
	
}
