module demo.java9.logging.ex01 {
	
	requires demo.util;
	
	exports demo.java9.logging.ex01;
	
	provides java.lang.System.LoggerFinder
	with demo.java9.logging.ex01.DemoLoggerFinder;
	
}
